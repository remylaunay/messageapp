from django.db import models
from django.contrib.auth.models import User

STATUS = (
    (0, "Draft"),
    (1, "Publish")
)


class Message(models.Model):
    sender = models.ForeignKey(User, on_delete=models.SET('NULL'), related_name='senderFK')
    target = models.ForeignKey(User, on_delete=models.SET('NULL'), related_name='targetFK')
    subject = models.TextField(max_length=100)
    content = models.TextField(max_length=500, blank=False)
    attachement = models.TextField(max_length=100)
    is_important = models.BooleanField(default=False)
    is_read = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.content


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.TextField(max_length=30, blank=False)
    avatar = models.TextField(max_length=100, blank=True)
