from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from rolepermissions.roles import assign_role
from rolepermissions.checkers import has_permission
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, resolve
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from Messages.models import Message


def index(request):
    arg = resolve(request.path_info).url_name
    listmessages = Message.objects.raw("CALL user_" + arg + "(" + str(request.user.id) + ")")
    paginator = Paginator(listmessages, 5)
    page = request.GET.get('page')
    messages = paginator.get_page(page)
    list_unread_messages = Message.objects.raw("CALL user_unread(" + str(request.user.id) + ")")
    list_drafted_messages = Message.objects.raw("CALL user_drafted(" + str(request.user.id) + ")")
    count_messages = listmessages.__len__()
    count_unread_messages = list_unread_messages.__len__()
    count_drafted_messages = list_drafted_messages.__len__()
    return render(request, 'inbox.html', {'messages': messages,
                                          'count': count_messages,
                                          'count_unread': count_unread_messages,
                                          'count_drafted': count_drafted_messages, })


def compose(request):
    return render(request, 'compose.html')


def contacts(request):
    user_list = Message.objects.raw("CALL user_list_active()")
    return render(request, 'contacts.html', {'users': user_list})