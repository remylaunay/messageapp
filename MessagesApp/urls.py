from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from Messages import views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', views.index, name='inbox'),
    url(r'^inbox$', views.index, name='inbox'),
    url(r'^sent$', views.index, name='sent'),
    url(r'^drafted$', views.index, name='drafted'),
    url(r'^deleted$', views.index, name='deleted'),
    url(r'^compose$', views.compose, name='compose'),
    url(r'^contacts$', views.contacts, name='contacts')]
